function lampChecker(number) {
  let state = false;

  for (let i = 1; i <= number; i++) {
    state = number % i === 0 ? !state : state;
  }

  if (state === true) return 'Lampu menyala';
  else return 'Lampu mati'
}