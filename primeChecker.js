function primeChecker(number) {
  if (number === 1) return 'Bukan bilangan prima';
  if (number === 2) return 'Bilangan prima';

  if (number % 3 !== 0 && number % 5 !== 0 && number % 7 !== 0) return 'Bilangan prima';
	
  return 'Bukan bilangan prima';
}